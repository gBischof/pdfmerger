# PDF Merger

## Overview
PDF Merger is a graphical user interface (GUI) for Poppler's pdfunite written in C++/Qt.

## Motivation and intent
This application is meant to provide a very simple way to concatenate PDF documents.
Originally I created the application for a user that was using a Windows application for this purpose that was not available for Linux after I provided them with a Linux installation. Despite being aware that there are numerous ways to concatenate PDF documents in a GNU/Linux environment, I felt that none of them were suitable to fully satisfy the following requirements for the user in mind. In particular:
* No overhead, no options, no whatever-not-really-essential
* Integration into the look & feel of the KDE Plasma Desktop
* Just should feel easy to do the job


## Build and installation

Run qmake, which creates the makefile and then make. In order to include the translations in the installation, run qmake again. Finally, install by 'make install' as root:


1.  qmake
2.  make
3.  qmake
4.  sudo make install


If you run Arch Linux (or another distribution based on Arch Linux and you can use the AUR), there is a package to simplify installation: https://aur.archlinux.org/packages/pdfmerger

## Help appreciated
Please let me know if you see any way I could improve the application, my coding style, if you see me fit for contributing to a particular other open source project or would like to contribute a patch or translation for PDF Merger.
