#include "successhandler.h"

#include <QSysInfo>
#include <QProcess>
#include <QPushButton>
#include <QMessageBox>
#include <QDebug>



SuccessHandler::SuccessHandler()
{

}


void SuccessHandler::showSuccessMessage(QWidget *parent, const QString filePath)
{
    SuccessHandler handler;
    FileManager fileManager = handler.determineFileManager();
    if(FileManager::Unknown != fileManager)
        handler.showCustomMessageBox(parent, fileManager, filePath);
    else
        QMessageBox::information(parent, messageboxHeadText(), messageboxSuccessText());

}

void SuccessHandler::showFailureMessage(QWidget *parent, const QString errors)
{
    SuccessHandler handler;
    QMessageBox::information(parent, messageboxHeadText(), tr("Document could not be created. Error:\n") + errors);
}


SuccessHandler::FileManager SuccessHandler::determineFileManager() const
{
    if("winnt" == QSysInfo::kernelType())
        return FileManager::Explorer;

    if("linux" == QSysInfo::kernelType())
    {
        QString mimeFileManager = runCommand("xdg-mime", {"query", "default", "inode/directory"});
        if(mimeFileManager.contains("dolphin"))
            return FileManager::Dolphin;
        if(mimeFileManager.contains("nautilus"))
            return FileManager::Nautilus;
    }

    return FileManager::Unknown;
}


QString SuccessHandler::runCommand(const QString command, const QStringList arguments) const
{
    QProcess process;
    process.start(command, arguments);
    process.waitForFinished();
    return process.readAllStandardOutput();
}

void SuccessHandler::showCustomMessageBox(QWidget *parent, const SuccessHandler::FileManager fileManager, const QString filePath) const
{
    QString openInFileManagerButtonText;
    switch(fileManager)
    {
        case FileManager::Dolphin:
            openInFileManagerButtonText = tr("Show file in Dolphin");
            break;
        case FileManager::Nautilus:
            openInFileManagerButtonText = tr("Show file in Nautilus");
            break;
        case FileManager::Explorer:
            openInFileManagerButtonText = tr("Show file in Explorer");
            break;
        default:
            openInFileManagerButtonText = "";
            break;
    }


    QMessageBox messageBox(parent);
    QPushButton *buttonShowInFileManager = messageBox.addButton(openInFileManagerButtonText, QMessageBox::AcceptRole);
    QPushButton *buttonOpenFile = messageBox.addButton(tr("Open file"), QMessageBox::AcceptRole);
    messageBox.addButton(QMessageBox::Ok);
    messageBox.setDefaultButton(QMessageBox::Ok);

    messageBox.setWindowTitle(messageboxHeadText());
    messageBox.setText(messageboxSuccessText());
    messageBox.setIcon(QMessageBox::Information);


    messageBox.exec();
    if(messageBox.clickedButton() == buttonShowInFileManager)
        launchFileManager(fileManager, filePath);
    if(messageBox.clickedButton() == buttonOpenFile)
        openFile(filePath);

}

void SuccessHandler::launchFileManager(const SuccessHandler::FileManager fileManager, const QString path) const
{

    QString fileManagerApp;
    QStringList fileManagerArgs;

    switch(fileManager)
    {
        case FileManager::Dolphin:
            fileManagerApp = "dolphin";
            fileManagerArgs.append("--select");
            fileManagerArgs.append(path);
            break;
        case FileManager::Nautilus:
            fileManagerApp = "nautilus";
            fileManagerArgs.append("-w");
            fileManagerArgs.append(path);
            break;
        case FileManager::Explorer:
            fileManagerApp = "explorer";
            fileManagerArgs.append("/select,\"" + path + "\"");
            break;

        default:
            fileManagerApp = "";
            break;

    }

    if(!fileManagerApp.isEmpty())
    {
        QProcess::startDetached(fileManagerApp, fileManagerArgs);
    }

}

void SuccessHandler::openFile(const QString filePath) const
{
    if("winnt" == QSysInfo::kernelType())
        QProcess::startDetached("\"" + filePath + "\"");
    else
        QProcess::startDetached("xdg-open", {filePath});
}

QString SuccessHandler::messageboxHeadText()
{
    return tr("Merge documents");
}

QString SuccessHandler::messageboxSuccessText()
{
    return tr("Document has been created.");
}

