#include "pdfmerger.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include <QPushButton>
#include <QList>
#include <QUrl>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QStyle>
#include <QSettings>
#include <QCloseEvent>

#include "successhandler.h"
#include "imageconverter.h"


const QString PdfMerger::SETTINGSORG = "PdfMerger";
const QString PdfMerger::SETTINGSAPP = "PdfMerger";

PdfMerger::PdfMerger(QWidget *parent)
    : QWidget(parent)
{
    restoreSettings();
    setupUi();
}

PdfMerger::~PdfMerger()
{

}


void PdfMerger::restoreSettings()
{
    QSettings settings(SETTINGSORG, SETTINGSAPP);

    restoreGeometry(settings.value("window.geometry").toByteArray());
    lastOpenDir = settings.value("session.lastOpenDir").toString();
    lastSaveDir = settings.value("session.lastSaveDir").toString();
}

void PdfMerger::persistSettings()
{
    QSettings settings(SETTINGSORG, SETTINGSAPP);
    settings.setValue("window.geometry", saveGeometry());
    settings.setValue("session.lastOpenDir", lastOpenDir);
    settings.setValue("session.lastSaveDir", lastSaveDir);

    settings.setValue("session.persistList", isPersistentList);
    if(isPersistentList)
    {
        QStringList list;
        foreach(QUrl url, fileSelector->getFilesInOrder())
            list.append(url.toString());
        settings.setValue("session.fileList", QVariant::fromValue(list));
    }
}


void PdfMerger::setupUi()
{

    QVBoxLayout *mainLayout = new QVBoxLayout(this);

    fileSelector = new MultipleFileSelector(this);
    connect(fileSelector, SIGNAL(listChanged(QList<QUrl>)), this, SLOT(fileListChanged()));
    mainLayout->addWidget(fileSelector);
    loadPersistedList();
    fileSelector->setLastOpenDirectory(lastOpenDir);

    QHBoxLayout *buttonLayout = new QHBoxLayout(this);

    QStyle *style = QApplication::style();

    QPushButton *mergeButton = new QPushButton(QIcon::fromTheme("document-save-as", style->standardIcon(QStyle::StandardPixmap::SP_DialogSaveButton)), tr("Merge"), this);
    connect(mergeButton, SIGNAL(clicked()), this, SLOT(merge()));
    buttonLayout->addWidget(mergeButton);

    QPushButton *saveListOption = new QPushButton(QIcon::fromTheme("lock", style->standardIcon(QStyle::StandardPixmap::SP_DialogDiscardButton)), "", this);
    saveListOption->setToolTip(tr("Persist file list"));
    saveListOption->setCheckable(true);
    saveListOption->setChecked(isPersistentList);
    connect(saveListOption, SIGNAL(toggled(bool)), this, SLOT(persistListToggled(bool)));
    buttonLayout->addWidget(saveListOption);

    buttonLayout->addStretch();


    QPushButton *appInfoButton = new QPushButton(QIcon::fromTheme("help-about", style->standardIcon(QStyle::StandardPixmap::SP_MessageBoxQuestion)), "", this);
    connect(appInfoButton, SIGNAL(clicked()), this, SLOT(showInfo()));
    buttonLayout->addWidget(appInfoButton);

    QPushButton *exitButton = new QPushButton(QIcon::fromTheme("application-exit", style->standardIcon(QStyle::StandardPixmap::SP_DialogCloseButton)), tr("Quit"), this);
    connect(exitButton, SIGNAL(clicked()), this, SLOT(close()));
    buttonLayout->addWidget(exitButton);

    mainLayout->addLayout(buttonLayout);

    setLayout(mainLayout);
}

void PdfMerger::loadPersistedList()
{
    QSettings settings(SETTINGSORG, SETTINGSAPP);
    isPersistentList = settings.value("session.persistList").toBool();

    if(!isPersistentList)
        return;

    QStringList fileList = settings.value("session.fileList").toStringList();
    fileSelector->setList(fileList);
}


void PdfMerger::closeEvent(QCloseEvent *event)
{
    if(checkChanges())
    {
        persistSettings();
        event->accept();
    }
    else
        event->ignore();
}

bool PdfMerger::checkChanges()
{
    return !hasUnsavedChanges || fileSelector->getFilesInOrder().isEmpty()
            || QMessageBox::question(this, tr("Discard list"), tr("The documents have not been merged yet. Do you want to quit anyway?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes;
}

void PdfMerger::merge()
{
    const QList<QUrl> fileList = fileSelector->getFilesInOrder();
    if(fileList.isEmpty())
        return;

    QUrl destination = QFileDialog::getSaveFileName(this, tr("Save merged document"), defaultSaveDestination(), tr("PDF document (*.pdf)"));
    if(destination.isEmpty() || !destination.isValid())
        return;

    lastSaveDir = destination.adjusted(QUrl::RemoveFilename).path();

    QStringList arguments;
    foreach(QUrl inputFile, fileList)
    {
        arguments.append(inputFile.path());
    }

    pdfUnite = new QProcess(this);
    connect(pdfUnite, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(mergeFinished(int)));

    progressBar = new QProgressDialog(this, Qt::Popup);
    progressBar->setCancelButton(0);
    progressBar->setRange(0, 0);
    progressBar->show();
    progressBar->setValue(0);

    convertImages(&arguments);
    arguments.append(destination.path());

    pdfUnite->start("pdfunite", arguments);
    if(pdfUnite->state() == QProcess::NotRunning)
        mergeFinished(PDFUNITEMISSING);

}


void PdfMerger::mergeFinished(const int result)
{
    progressBar->setRange(100, 100);
    progressBar->setValue(100);
    progressBar->hide();
    delete progressBar;

    QString generatedFilePath = pdfUnite->arguments().last();

    if(0 == result )
    {
        SuccessHandler::showSuccessMessage(this, generatedFilePath);
        hasUnsavedChanges = false;
    }
    else {
        QString errors;
        if(result == PDFUNITEMISSING)
            errors = tr("pdfunite could not be called.");
        else
        {
            pdfUnite->waitForReadyRead();
            errors = pdfUnite->readAllStandardError();
        }
        SuccessHandler::showFailureMessage(this, errors);
    }

    delete pdfUnite;

}

QString PdfMerger::defaultSaveDestination()
{
    if(lastSaveDir.isNull() || lastSaveDir.isEmpty())
        return QDir::homePath();

    return lastSaveDir;
}

void PdfMerger::persistListToggled(bool checked)
{
    isPersistentList = checked;
}


void PdfMerger::fileListChanged()
{
    hasUnsavedChanges = true;
    lastOpenDir = fileSelector->getLastOpenLocation();
}


void PdfMerger::showInfo()
{
    QMessageBox::about(this, tr("About PDF Merger"), appInfo);
}


void PdfMerger::convertImages(QStringList *fileList)
{
    ImageConverter::convert(fileList);
}


