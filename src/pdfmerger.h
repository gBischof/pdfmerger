#ifndef PDFMERGER_H
#define PDFMERGER_H

#include <QWidget>
#include <QString>
#include <QListView>
#include <QStringListModel>
#include <QPushButton>
#include <QProcess>
#include <QProgressDialog>


#include "multiplefileselector.h"

class PdfMerger : public QWidget
{
    Q_OBJECT

public:
    PdfMerger(QWidget *parent = 0);
    ~PdfMerger();

    const QString appInfo = tr("<b>PDF Merger</b><br>") +
                            tr("Version ") + APP_VERSION + "<br><br>" +
                            tr("Author:") + " Gerald Bischof<br>" +
                            tr("<p>PDF Merger is a simple graphical frontend for Popplers' pdfunite command line tool. If poppler is not installed, this application is of no use.</p>") +
                            tr("<p>This application is published under the terms of the GNU General Public Licence v3 and comes as is without any warranties.</p>");

protected:
    void closeEvent(QCloseEvent *event) override;
    void convertImages(QStringList *fileList);
    static const QString SETTINGSORG;
    static const QString SETTINGSAPP;

private:

    static const int PDFUNITEMISSING = -3500;

    bool hasUnsavedChanges = false;

    MultipleFileSelector *fileSelector;
    QProcess *pdfUnite;
    QProgressDialog *progressBar;
    bool isPersistentList = false;
    QString lastSaveDir = "";
    QString lastOpenDir = "";

    void restoreSettings();
    void setupUi();
    bool checkChanges();
    void loadPersistedList();
    void persistSettings();

    QString defaultSaveDestination();

private slots:

    void merge();
    void fileListChanged();
    void showInfo();

    void mergeFinished(const int result);

    void persistListToggled(bool checked);
};

#endif // PDFMERGER_H
