#ifndef MULTIPLEFILESELECTOR_H
#define MULTIPLEFILESELECTOR_H

#include <QWidget>
#include <QListView>
#include <QStringList>
#include <QStringListModel>
#include <QUrl>
#include <QDir>
#include <QList>

class MultipleFileSelector : public QWidget
{

    Q_OBJECT

public:
    MultipleFileSelector(QWidget *parent = 0);
    ~MultipleFileSelector();

    QList<QUrl> getFilesInOrder() const;
    QStringList setList(const QStringList fileList);
    void setLastOpenDirectory(const QString directoryPath);
    QString getLastOpenLocation() const;

signals:
    void listChanged(QList<QUrl> currentList);

private:

    QListView *mergeFileList;
    QStringListModel *mergeFileModel;
    QStringList fileList;

    QUrl outputUrl;
    QString lastOpenLocation = QDir::homePath();

    void addFiles(const QList<QUrl> files);
    const QString getSelectedItem() const;
    QStringList *moveItem(QStringList *source, const QString item, const int direction) const;
    void moveSelectedItem(const int direction);

    void setup();
    static bool isPdfUrl(const QUrl url);

protected:
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;


private slots:
    void addFiles();
    void removeFile();
    void clearList();
    void moveUp();
    void moveDown();


};

#endif // MULTIPLEFILESELECTOR_H
