#include "pdfmerger.h"
#include <QApplication>
#include <QTranslator>
#include <QSysInfo>
#include <QIcon>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    const QString iconPath = "linux" == QSysInfo::kernelType() ? "/usr/share/PdfMerger/icons/" : "./";
    a.setWindowIcon(QIcon(iconPath + "pdfmerger.svg"));

    const QString localizationPath = "linux" == QSysInfo::kernelType() ? "/usr/share/PdfMerger/translations/" : "./";
    QTranslator translator;
    translator.load(localizationPath + "PdfMerger_" + QLocale::system().name());
    a.installTranslator(&translator);

    PdfMerger w;
    w.setWindowTitle("PDF Merger");
    w.show();

    return a.exec();
}
